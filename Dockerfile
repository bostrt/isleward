FROM node:latest

WORKDIR /usr/src/isleward
COPY . . 

WORKDIR /usr/src/isleward/src/server
RUN npm install
# HACK
RUN chmod a+rwx -R /usr/src/isleward

EXPOSE 4000


CMD ["npm", "start"]

